#!/usr/bin/python

# --------------------------------------------------------------------------
# Copyright (c) 2012, University of Cambridge Computing Service
#
# This file is part of the Lookup/Ibis client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Dean Rasheed (dev-group@ucs.cam.ac.uk)
# --------------------------------------------------------------------------

from datetime import date
import os
import re
import unittest
import urllib.request

try:
    from ibisclient import (createLocalConnection, createTestConnection, IbisAttribute,
                            IbisMethods, IbisException, PersonMethods, InstitutionMethods,
                            GroupMethods)
except ImportError:
    import sys
    _script_dir = os.path.realpath(os.path.dirname(__file__))
    _ibisclient_dir = os.path.realpath(os.path.join(_script_dir, ".."))
    sys.path.append(_ibisclient_dir)
    from ibisclient import (createLocalConnection, createTestConnection, IbisAttribute,
                            IbisMethods, IbisException, PersonMethods, InstitutionMethods,
                            GroupMethods)

# Get Lookup credentials from the environment
LOOKUP_TEST_SERVER_VIEWER_USERNAME = os.getenv("LOOKUP_TEST_SERVER_VIEWER_USERNAME", "anonymous")
LOOKUP_TEST_SERVER_VIEWER_PASSWORD = os.getenv("LOOKUP_TEST_SERVER_VIEWER_PASSWORD", "")
LOOKUP_TEST_SERVER_EDITOR_USERNAME = os.environ["LOOKUP_TEST_SERVER_EDITOR_USERNAME"]
LOOKUP_TEST_SERVER_EDITOR_PASSWORD = os.environ["LOOKUP_TEST_SERVER_EDITOR_PASSWORD"]

# Test on local server or lookup-test?
local = False

# Run the editing tests or not?
run_edit_tests = False

# Globals initialised once and re-used in each test
conn = None
m = None
pm = None
im = None
gm = None
initialised = False


class IbisUnitTests(unittest.TestCase):
    def setUp(self):
        global conn, m, pm, im, gm, initialised

        if not initialised:
            if local:
                conn = createLocalConnection()
            else:
                conn = createTestConnection()
                conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
                conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

            m = IbisMethods(conn)
            pm = PersonMethods(conn)
            im = InstitutionMethods(conn)
            gm = GroupMethods(conn)
            initialised = True

    # --------------------------------------------------------------------
    # Ibis tests.
    # --------------------------------------------------------------------

    def test_get_version(self):
        version = m.getVersion()
        self.assertIsNotNone(version)
        self.assertTrue(re.match("^[0-9]+[.][0-9]+$", version))

    def test_get_last_transaction_id(self):
        lastTransactionId = m.getLastTransactionId()
        self.assertTrue(lastTransactionId > 900000)

    # --------------------------------------------------------------------
    # Person tests.
    # --------------------------------------------------------------------

    def test_person_attribute_schemes(self):
        schemes = pm.allAttributeSchemes()
        self.assertTrue(len(schemes) > 10)
        self.assertEqual("displayName", schemes[0].schemeid)

    def test_all_people(self):
        people = pm.allPeople(False, None, 10, None)

        self.assertEqual(10, len(people))
        self.assertEqual("aa", people[0].identifier.value[0:2])

        people = pm.allPeople(False, "dar17", 10, None)
        self.assertEqual(10, len(people))
        self.assertTrue(people[0].identifier.value > "dar17")

        id8 = people[8].identifier.value
        id9 = people[9].identifier.value
        people = pm.allPeople(False, id8, 10, None)
        self.assertEqual(10, len(people))
        self.assertEqual(id9, people[0].identifier.value)

    def test_no_such_person(self):
        person = pm.getPerson("crsid", "dar1734toolong")
        self.assertIsNone(person)

    def test_no_such_identifier_scheme(self):
        person = pm.getPerson("crs", "dar17")
        self.assertIsNone(person)

    def test_get_person_details(self):
        person = pm.getPerson("crsid", "dar17")
        self.assertEqual("Rasheed", person.surname)
        self.assertNotEqual("staff", person.misAffiliation)
        self.assertTrue(person.is_staff())
        self.assertFalse(person.is_student())

    def test_get_person_identifiers(self):
        person = pm.getPerson("crsid", "dar17", "all_identifiers")
        self.assertEqual("crsid", person.identifiers[0].scheme)
        self.assertEqual("dar17", person.identifiers[0].value)

    def test_get_person_title(self):
        person = pm.getPerson("crsid", "dar17", "title")
        self.assertEqual("title", person.attributes[0].scheme)
        self.assertEqual("Database Administrator & Developer", person.attributes[0].value)

    def test_get_person_attributes(self):
        attrs = pm.getAttributes("crsid", "rjd4", "email,title")
        self.assertEqual("title", attrs[0].scheme)
        self.assertEqual("Head of Online Services division, UCS", attrs[0].value)
        self.assertEqual("email", attrs[1].scheme)

        attr = pm.getAttribute("crsid", "rjd4", attrs[1].attrid)
        self.assertEqual(attrs[1].scheme, attr.scheme)
        self.assertEqual(attrs[1].value, attr.value)

    def test_get_person_insts_and_groups(self):
        person = pm.getPerson("crsid", "mug99", "all_insts,all_groups")
        self.assertEqual("UISTEST", person.institutions[0].instid)
        self.assertEqual("uistest-members", person.groups[0].name)

    def test_get_person_inst_managers(self):
        person = pm.getPerson("crsid", "mug99", "all_insts.managed_by_groups.all_members")

        inst = person.institutions[0]
        self.assertEqual("UISTEST", inst.instid)

        mgrGroup = inst.managedByGroups[0]
        self.assertEqual("uistest-editors", mgrGroup.name)
        print(mgrGroup.members)
        self.assertEqual("R.J. Dowling", mgrGroup.members[0].registeredName)

    def test_list_people(self):
        people = pm.listPeople("ijl20,rjd4,pms52,dar17,prb34,dcs38", "email")
        self.assertEqual(6, len(people))
        self.assertEqual("dar17", people[0].identifier.value)
        self.assertEqual("dcs38@cam.ac.uk", people[1].attributes[0].value)
        self.assertEqual("Lewis", people[2].surname)
        self.assertEqual("P.M. Shore", people[3].registeredName)
        self.assertEqual("prb34", people[4].identifier.value)
        self.assertEqual("Dowling", people[5].surname)

    def test_person_search(self):
        people = pm.search("ian lewis", False, False, None, None, 0, 100)
        self.assertEqual(1, len(people))
        self.assertEqual("ijl20", people[0].identifier.value)

        people = pm.search("dowling UIS", False, False,
                           "staff", "surname,jdInstid", 0, 100, None, "address,title")
        self.assertEqual(2, len(people))
        self.assertEqual("rjd4", people[0].identifier.value)
        self.assertEqual("Head of Online Services division, UCS", people[0].attributes[0].value)
        self.assertTrue(people[0].attributes[1].value.find("New Museums Site") != -1)

        people = pm.search("dar54", False, True, None, None, 0, 100)
        self.assertEqual(1, len(people))
        self.assertEqual("dar54", people[0].identifier.value)

    def test_person_search_count(self):
        count = pm.searchCount("j smith", False, False)
        self.assertTrue(count > 10)

    def test_person_lql_search(self):
        people = pm.search("person: in inst(uis) and surname=Dowling", False, False,
                           None, None, 0, 100, None, "title")
        self.assertEqual(1, len(people))
        self.assertEqual("rjd4", people[0].identifier.value)
        self.assertEqual("Head of Online Services division, UCS", people[0].attributes[0].value)

        people = pm.search("person: dar54", False, False,
                           None, None, 0, 100, None, None)
        self.assertEqual(0, len(people))

        people = pm.search("person: dar54", False, True,
                           None, None, 0, 100, None, None)
        self.assertEqual(1, len(people))
        self.assertEqual("dar54", people[0].identifier.value)

    def test_person_lql_search_count(self):
        count = pm.searchCount("person: rjd4", False, False, None, None)
        self.assertEqual(1, count)

        count = pm.searchCount("person: dar54", False, False, None, None)
        self.assertEqual(0, count)

        count = pm.searchCount("person: dar54", False, True, None, None)
        self.assertEqual(1, count)

    def test_is_person_member_of_inst(self):
        self.assertTrue(pm.isMemberOfInst("crsid", "dar17", "UIS"))
        self.assertFalse(pm.isMemberOfInst("crsid", "dar17", "ENG"))
        self.assertFalse(pm.isMemberOfInst("crs", "dar1734-sdfr", "CS"))

    def test_is_person_member_of_group(self):
        self.assertTrue(pm.isMemberOfGroup("crsid", "rjd4", "100656"))
        self.assertTrue(pm.isMemberOfGroup("crsid", "rjd4", "cs-editors"))
        self.assertFalse(pm.isMemberOfGroup("crsid", "dar99", "100656"))
        self.assertFalse(pm.isMemberOfGroup("crsid", "rjd4", "3g3rsfh"))
        self.assertFalse(pm.isMemberOfGroup("crsid", "34sf_rjd", "3g3rsfh"))
        self.assertFalse(pm.isMemberOfGroup("crs", "34sf_rjd", "3g3rsfh"))

    def test_get_persons_insts(self):
        person = pm.getPerson("crsid", "ijl20", "all_insts")
        insts = pm.getInsts("crsid", "ijl20")

        self.assertEqual(2, len(person.institutions))
        self.assertEqual(2, len(insts))
        for i in range(0, len(insts)):
            i1 = person.institutions[i]
            i2 = insts[i]
            self.assertEqual(i1.instid, i2.instid)
            self.assertEqual(i1.name, i2.name)

    def test_get_persons_managed_insts(self):
        person = pm.getPerson("crsid", "rjd4", "all_groups.manages_insts")
        insts = pm.getManagedInsts("crsid", "rjd4")

        managedInsts = []
        for group in person.groups:
            for managedInst in group.managesInsts:
                managedInsts.append(managedInst)

        self.assertEqual(1, len(managedInsts))
        self.assertEqual(1, len(insts))
        for i in range(0, len(insts)):
            i1 = managedInsts[i]
            i2 = insts[i]
            self.assertEqual(i1.instid, i2.instid)
            self.assertEqual(i1.name, i2.name)

    def test_get_persons_groups(self):
        person = pm.getPerson("crsid", "dar99", "all_groups")
        groups = pm.getGroups("crsid", "dar99")

        self.assertTrue(len(person.groups) == 0)
        self.assertTrue(len(person.groups) == len(groups))
        for i in range(0, len(groups)):
            g1 = person.groups[i]
            g2 = groups[i]
            self.assertEqual(g1.groupid, g2.groupid)
            self.assertEqual(g1.name, g2.name)

    def test_get_persons_managed_groups(self):
        person = pm.getPerson("crsid", "dar99", "all_groups.manages_groups")
        groups = pm.getManagedGroups("crsid", "dar99")

        managedGroups = []
        for group in person.groups:
            for managedGroup in group.managesGroups:
                managedGroups.append(managedGroup)

        managedGroups.sort(key=lambda group: group.groupid)

        self.assertTrue(len(managedGroups) == len(groups))
        for i in range(0, len(groups)):
            g1 = managedGroups[i]
            g2 = groups[i]
            self.assertEqual(g1.groupid, g2.groupid)
            self.assertEqual(g1.name, g2.name)

    def test_person_edit(self):
        if not run_edit_tests:
            return

        try:
            conn.set_username(LOOKUP_TEST_SERVER_EDITOR_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_EDITOR_PASSWORD)

            # Delete any test emails from previous runs
            person = pm.getPerson("crsid", "dar99", "email")
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    pm.deleteAttribute("crsid", "dar99", attr.attrid,
                                       "Unit test: delete existing test emails")

            person = pm.getPerson("crsid", "dar99", "email")
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.fail("There should be no test emails left")

            # Test adding a new email
            newAttr = IbisAttribute()
            newAttr.scheme = "email"
            newAttr.value = "dev-group@ucs.cam.ac.uk"
            newAttr.comment = "Unit testing"
            newAttr.instid = "CS"
            newAttr.effectiveFrom = date.today()

            newAttr = pm.addAttribute("crsid", "dar99", newAttr, 0, True,
                                      "Unit test: add a test email")
            self.assertEqual("dev-group@ucs.cam.ac.uk", newAttr.value)
            self.assertEqual("Unit testing", newAttr.comment)
            self.assertEqual("CS", newAttr.instid)
            self.assertIsNotNone(newAttr.effectiveFrom)
            self.assertIsNone(newAttr.effectiveTo)
            self.assertEqual("100668", newAttr.owningGroupid)

            person = pm.getPerson("crsid", "dar99", "email")

            found = False
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.assertEqual(newAttr.attrid, attr.attrid)
                    self.assertEqual(newAttr.value, attr.value)
                    self.assertEqual(newAttr.comment, attr.comment)
                    self.assertEqual(newAttr.instid, attr.instid)
                    self.assertEqual(newAttr.effectiveFrom, attr.effectiveFrom)
                    self.assertEqual(newAttr.effectiveTo, attr.effectiveTo)
                    self.assertEqual(newAttr.owningGroupid, attr.owningGroupid)
                    self.assertFalse(found)
                    found = True
            self.assertTrue(found)

            # Test updating the new email
            newAttr.value = "foo@bar.com"
            newAttr.comment = "Unit test update"
            newAttr.instid = "CSTEST"
            newAttr.effectiveFrom = None
            newAttr.effectiveTo = date.today()
            updatedAttr = pm.updateAttribute("crsid", "dar99", newAttr.attrid,
                                             newAttr, "Unit test: update email")
            self.assertEqual(newAttr.value, updatedAttr.value)
            self.assertEqual(newAttr.comment, updatedAttr.comment)
            self.assertEqual(newAttr.instid, updatedAttr.instid)
            self.assertIsNone(updatedAttr.effectiveFrom)
            self.assertIsNotNone(updatedAttr.effectiveTo)
            self.assertEqual(newAttr.owningGroupid, updatedAttr.owningGroupid)

            person = pm.getPerson("crsid", "dar99", "email")

            found = False
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.assertEqual(updatedAttr.attrid, attr.attrid)
                    self.assertEqual(updatedAttr.value, attr.value)
                    self.assertEqual(updatedAttr.comment, attr.comment)
                    self.assertEqual(updatedAttr.instid, attr.instid)
                    self.assertEqual(updatedAttr.effectiveFrom, attr.effectiveFrom)
                    self.assertEqual(updatedAttr.effectiveTo, attr.effectiveTo)
                    self.assertEqual(updatedAttr.owningGroupid, attr.owningGroupid)
                    self.assertFalse(found)
                    found = True
            self.assertTrue(found)

            # Test deleting the new email
            deleted = pm.deleteAttribute("crsid", "dar99", updatedAttr.attrid,
                                         "Unit test: delete email")
            self.assertTrue(deleted)

            person = pm.getPerson("crsid", "dar99", "email")

            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.fail("There should be no test emails left")
        finally:
            conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

    def test_person_edit_image(self):
        if not run_edit_tests:
            return

        try:
            conn.set_username(LOOKUP_TEST_SERVER_EDITOR_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_EDITOR_PASSWORD)

            # The image to use for testing
            f = urllib.request.urlopen("http://www.lookup.cam.ac.uk/images/ibis.jpg")
            imageData = f.read()
            f.close()

            # Delete any test images from previous runs
            person = pm.getPerson("crsid", "dar99", "jpegPhoto")
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    pm.deleteAttribute("crsid", "dar99", attr.attrid,
                                       "Unit test: delete existing test photos")

            person = pm.getPerson("crsid", "dar99", "jpegPhoto")
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.fail("There should be no test photos left")

            # Test adding a new photo
            newAttr = IbisAttribute()
            newAttr.scheme = "jpegPhoto"
            newAttr.binaryData = imageData
            newAttr.comment = "Unit testing"

            newAttr = pm.addAttribute("crsid", "dar99", newAttr, 0, True,
                                      "Unit test: add a test photo")
            self.assertIsNone(newAttr.value)
            self.assertEqual(imageData, newAttr.binaryData)
            self.assertEqual("Unit testing", newAttr.comment)
            self.assertEqual("100668", newAttr.owningGroupid)

            person = pm.getPerson("crsid", "dar99", "jpegPhoto")

            found = False
            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.assertEqual(newAttr.attrid, attr.attrid)
                    self.assertIsNone(attr.value)
                    self.assertEqual(imageData, attr.binaryData)
                    self.assertEqual(newAttr.comment, attr.comment)
                    self.assertEqual(newAttr.instid, attr.instid)
                    self.assertEqual(newAttr.effectiveFrom, attr.effectiveFrom)
                    self.assertEqual(newAttr.effectiveTo, attr.effectiveTo)
                    self.assertEqual(newAttr.owningGroupid, attr.owningGroupid)
                    self.assertFalse(found)
                    found = True
            self.assertTrue(found)

            # Test deleting the new photo
            deleted = pm.deleteAttribute("crsid", "dar99", newAttr.attrid,
                                         "Unit test: delete photo")
            self.assertTrue(deleted)

            person = pm.getPerson("crsid", "dar99", "jpegPhoto")

            for attr in person.attributes:
                if attr.owningGroupid == "100668":
                    self.fail("There should be no test photos left")
        finally:
            conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

    def test_modified_people(self):
        # Note: transactions 2..1047 are the same on Lookup and lookup-test
        people = pm.modifiedPeople(937, 938, None, False, False, False, None)
        self.assertEqual(1, len(people))
        self.assertEqual("v4500", people[0].identifier.value)

        people = pm.modifiedPeople(752, 753, None, False, False, False, None)
        self.assertEqual(0, len(people))

        people = pm.modifiedPeople(752, 753, None, True, False, False, None)
        self.assertEqual(2, len(people))
        self.assertEqual("cr10001", people[0].identifier.value)
        self.assertEqual("gar34", people[1].identifier.value)

        people = pm.modifiedPeople(752, 753, "cr10001", False, False, False, None)
        self.assertEqual(0, len(people))

        people = pm.modifiedPeople(752, 753, "cr10001", True, False, False, None)
        self.assertEqual(1, len(people))
        self.assertEqual("cr10001", people[0].identifier.value)

        people = pm.modifiedPeople(752, 753, "cr10002", True, False, False, None)
        self.assertEqual(0, len(people))

    # --------------------------------------------------------------------
    # Institution tests.
    # --------------------------------------------------------------------

    def test_inst_attribute_schemes(self):
        schemes = im.allAttributeSchemes()
        self.assertTrue(len(schemes) > 10)
        self.assertEqual("name", schemes[0].schemeid)

    def test_no_such_inst(self):
        inst = im.getInst("54jkn4")
        self.assertIsNone(inst)

    def test_get_inst_details(self):
        inst = im.getInst("CS")
        self.assertEqual("UCS", inst.acronym)
        self.assertEqual("University Computing Service", inst.name)

    def test_get_inst_email_and_phone_numbers(self):
        inst = im.getInst("CS", "email,phone_numbers")

        foundEmail = False
        foundPhoneNumber = False
        for attr in inst.attributes:
            if attr.scheme == "email" and\
               attr.value == "reception@ucs.cam.ac.uk":
                foundEmail = True
            if attr.scheme == "universityPhone" and\
               attr.value == "34600":
                foundPhoneNumber = True
        self.assertTrue(foundEmail)
        self.assertTrue(foundPhoneNumber)

    def test_get_inst_attributes(self):
        attrs = im.getAttributes("CS", "acronym,email")
        self.assertEqual("acronym", attrs[0].scheme)
        self.assertEqual("UCS", attrs[0].value)
        self.assertEqual("email", attrs[1].scheme)

        attr = im.getAttribute("CS", attrs[0].attrid)
        self.assertEqual(attrs[0].scheme, attr.scheme)
        self.assertEqual(attrs[0].value, attr.value)

    def test_get_inst_members(self):
        inst = im.getInst("UIS", "all_members")
        people = im.getMembers("UIS")

        self.assertTrue(len(people) > 100)
        self.assertTrue(len(inst.members) == len(people))
        for i in range(0, len(people)):
            p1 = inst.members[i]
            p2 = people[i]
            self.assertEqual(p1.identifier.scheme, p2.identifier.scheme)
            self.assertEqual(p1.identifier.value, p2.identifier.value)
            self.assertEqual(p1.displayName, p2.displayName)

    def test_get_inst_members_jdInstid(self):
        inst = im.getInst("UIS", "all_members.jdInstid")
        people = im.getMembers("UIS", "jdInstid")

        self.assertTrue(len(inst.members) == len(people))
        for i in range(0, len(people)):
            p1 = inst.members[i]
            p2 = people[i]
            self.assertEqual(p1.identifier.scheme, p2.identifier.scheme)
            self.assertEqual(p1.identifier.value, p2.identifier.value)
            self.assertEqual(p1.attributes[0].value, p2.attributes[0].value)

    def test_get_inst_cancelled_members(self):
        people = im.getCancelledMembers("CS")

        self.assertTrue(len(people) > 30)

        found = False
        for person in people:
            if person.identifier.scheme == "crsid" and\
               person.identifier.value == "dar54":
                self.assertEqual("Dr D.A. Rasheed", person.registeredName)
                found = True
        self.assertTrue(found)

    def test_get_inst_parents(self):
        inst = im.getInst("CHURCH", "parent_insts")
        self.assertEqual("COLL", inst.parentInsts[0].instid)

    def test_get_inst_parents_children(self):
        inst = im.getInst("CHURCH", "parent_insts.child_insts")
        self.assertEqual("COLL", inst.parentInsts[0].instid)
        self.assertEqual("FTHEO", inst.parentInsts[0].childInsts[0].instid)
        self.assertEqual("CHRISTS", inst.parentInsts[0].childInsts[1].instid)
        self.assertEqual("CHURCH", inst.parentInsts[0].childInsts[2].instid)
        self.assertEqual("CLARE", inst.parentInsts[0].childInsts[3].instid)
        self.assertEqual("CLAREH", inst.parentInsts[0].childInsts[4].instid)

        self.assertTrue(inst.parentInsts[0].childInsts[2] == inst)

    def test_get_inst_groups(self):
        inst = im.getInst("CS", "inst_groups,managed_by_groups.managed_by_groups,members_groups")
        self.assertEqual("cs-members", inst.groups[0].name)
        self.assertEqual("cs-managers", inst.groups[1].name)
        self.assertEqual("cs-editors", inst.groups[2].name)
        self.assertEqual("cs-members", inst.membersGroups[0].name)
        self.assertEqual("cs-editors", inst.managedByGroups[0].name)
        self.assertEqual("cs-managers", inst.managedByGroups[0].managedByGroups[0].name)

        self.assertTrue(inst.membersGroups[0] == inst.groups[0])
        self.assertTrue(inst.managedByGroups[0] == inst.groups[2])
        self.assertTrue(inst.managedByGroups[0].managedByGroups[0] == inst.groups[1])

    def test_inst_fetch_depth(self):
        f = ".child_insts.parent_insts"
        fok = (f+f+f+f+f)[1:]
        ferr = fok+".child_insts"

        inst = im.getInst("UIS", fok)
        cpInst = inst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)
        cpInst = cpInst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)
        cpInst = cpInst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)
        cpInst = cpInst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)
        cpInst = cpInst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)
        cpInst = cpInst.childInsts[0].parentInsts[0]
        self.assertEqual("UIS", cpInst.instid)

        try:
            inst = im.getInst("UIS", ferr)
            self.fail("Should have failed due to fetch depth too large")
        except IbisException as e:
            self.assertEqual(500, e.get_error().status)
            self.assertEqual("Nested fetch depth too large.", e.get_error().message)

    def test_list_insts(self):
        insts = im.listInsts("CS,CSTEST,dsrlgnr,ENG", "email")
        self.assertEqual(3, len(insts))
        self.assertEqual("CS", insts[0].instid)
        self.assertEqual("reception@ucs.cam.ac.uk", insts[0].attributes[0].value)
        self.assertEqual("CSTEST", insts[1].instid)
        self.assertEqual("Department of Engineering", insts[2].name)

    def test_inst_search(self):
        insts = im.search("information services", False, False,
                          None, 0, 100, "instid")
        self.assertEqual("UIS", insts[0].instid)

        insts = im.search("CB3 0JG", False, False,
                          "address", 0, 100, None, "phone_numbers")
        self.assertEqual(1, len(insts))
        self.assertEqual("GIRTON", insts[0].instid)
        self.assertEqual("38999", insts[0].attributes[0].value)

    def test_inst_search_count(self):
        count = im.searchCount("computing")
        self.assertTrue(count > 5)

    def test_inst_lql_search(self):
        insts = im.search("inst: parent of (uistest)", False, False,
                          None, 0, 100, None, None)
        self.assertEqual("UIS", insts[0].instid)

        insts = im.search("inst: address ~ CB3 0JG", False, False,
                          None, 0, 100, None, "phone_numbers")
        self.assertEqual(1, len(insts))
        self.assertEqual("GIRTON", insts[0].instid)
        self.assertEqual("38999", insts[0].attributes[0].value)

    def test_inst_lql_search_count(self):
        count = im.searchCount("inst: UIS", False, False, None)
        self.assertEqual(1, count)

        count = im.searchCount("inst: CS", False, False, None)
        self.assertEqual(0, count)

        count = im.searchCount("inst: CS", False, True, None)
        self.assertEqual(1, count)

    def test_get_inst_contact_rows(self):
        inst = im.getInst("CS", "contact_rows.jdInstid")
        contactRows = im.getContactRows("CS", "jdInstid")

        self.assertTrue(len(contactRows) > 20)
        self.assertEqual("Enquiries and Reception", contactRows[0].description)
        self.assertEqual("reception@ucs.cam.ac.uk", contactRows[0].emails[0])
        self.assertEqual("universityPhone", contactRows[0].phoneNumbers[0].phoneType)
        self.assertEqual("34600", contactRows[0].phoneNumbers[0].number)
        self.assertEqual("Director", contactRows[2].description)
        self.assertEqual("Ian Lewis", contactRows[2].people[0].displayName)
        self.assertEqual("UIS", contactRows[2].people[0].attributes[0].value)

        self.assertEqual(len(inst.contactRows), len(contactRows))
        for i in range(0, len(contactRows)):
            r1 = contactRows[i]
            r2 = inst.contactRows[i]
            self.assertEqual(r1.bold, r2.bold)
            self.assertEqual(r1.italic, r2.italic)
            self.assertEqual(r1.description, r2.description)

            self.assertEqual(len(r1.addresses), len(r2.addresses))
            for j in range(0, len(r1.addresses)):
                self.assertEqual(r1.addresses[j], r2.addresses[j])

            self.assertEqual(len(r1.emails), len(r2.emails))
            for j in range(0, len(r1.emails)):
                self.assertEqual(r1.emails[j], r2.emails[j])

            self.assertEqual(len(r1.people), len(r2.people))
            for j in range(0, len(r1.people)):
                self.assertEqual(r1.people[j].identifier.value, r2.people[j].identifier.value)
                self.assertEqual(r1.people[j].displayName, r2.people[j].displayName)
                self.assertEqual(r1.people[j].attributes[0].value,
                                 r2.people[j].attributes[0].value)

            self.assertEqual(len(r1.phoneNumbers), len(r2.phoneNumbers))
            for j in range(0, len(r1.phoneNumbers)):
                self.assertEqual(r1.phoneNumbers[j].phoneType, r2.phoneNumbers[j].phoneType)
                self.assertEqual(r1.phoneNumbers[j].number, r2.phoneNumbers[j].number)
                self.assertEqual(r1.phoneNumbers[j].comment, r2.phoneNumbers[j].comment)

            self.assertEqual(len(r1.webPages), len(r2.webPages))
            for j in range(0, len(r1.webPages)):
                self.assertEqual(r1.webPages[j].url, r2.webPages[j].url)
                self.assertEqual(r1.webPages[j].label, r2.webPages[j].label)

    def test_inst_edit(self):
        if not run_edit_tests:
            return

        try:
            conn.set_username(LOOKUP_TEST_SERVER_EDITOR_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_EDITOR_PASSWORD)

            # Delete any test emails from previous runs
            inst = im.getInst("CSTEST", "email")
            for attr in inst.attributes:
                im.deleteAttribute("CSTEST", attr.attrid,
                                   "Unit test: delete existing test emails")

            inst = im.getInst("CSTEST", "email")
            self.assertEqual(0, len(inst.attributes))

            # Test adding a new email
            newAttr = IbisAttribute()
            newAttr.scheme = "email"
            newAttr.value = "cstest@ucs.cam.ac.uk"
            newAttr.comment = "Unit testing"
            newAttr.effectiveFrom = date.today()

            newAttr = im.addAttribute("CSTEST", newAttr, 0, True,
                                      "Unit test: add a test email")
            self.assertEqual("cstest@ucs.cam.ac.uk", newAttr.value)
            self.assertEqual("Unit testing", newAttr.comment)
            self.assertIsNotNone(newAttr.effectiveFrom)
            self.assertIsNone(newAttr.effectiveTo)

            inst = im.getInst("CSTEST", "email")

            found = False
            for attr in inst.attributes:
                self.assertEqual(newAttr.attrid, attr.attrid)
                self.assertEqual(newAttr.value, attr.value)
                self.assertEqual(newAttr.comment, attr.comment)
                self.assertEqual(newAttr.effectiveFrom, attr.effectiveFrom)
                self.assertEqual(newAttr.effectiveTo, attr.effectiveTo)
                self.assertFalse(found)
                found = True
            self.assertTrue(found)

            # Test updating the new email
            newAttr.value = "foo@bar.com"
            newAttr.comment = "Unit test update"
            newAttr.effectiveFrom = None
            newAttr.effectiveTo = date.today()
            updatedAttr = im.updateAttribute("CSTEST", newAttr.attrid,
                                             newAttr, "Unit test: update email")
            self.assertEqual(newAttr.value, updatedAttr.value)
            self.assertEqual(newAttr.comment, updatedAttr.comment)
            self.assertIsNone(updatedAttr.effectiveFrom)
            self.assertIsNotNone(updatedAttr.effectiveTo)

            inst = im.getInst("CSTEST", "email")

            found = False
            for attr in inst.attributes:
                self.assertEqual(updatedAttr.attrid, attr.attrid)
                self.assertEqual(updatedAttr.value, attr.value)
                self.assertEqual(updatedAttr.comment, attr.comment)
                self.assertEqual(updatedAttr.effectiveFrom, attr.effectiveFrom)
                self.assertEqual(updatedAttr.effectiveTo, attr.effectiveTo)
                self.assertFalse(found)
                found = True
            self.assertTrue(found)

            # Test deleting the new email
            deleted = im.deleteAttribute("CSTEST", updatedAttr.attrid,
                                         "Unit test: delete email")
            self.assertTrue(deleted)

            inst = im.getInst("CSTEST", "email")
            self.assertEqual(0, len(inst.attributes))
        finally:
            conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

    def test_inst_edit_image(self):
        if not run_edit_tests:
            return

        try:
            conn.set_username(LOOKUP_TEST_SERVER_EDITOR_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_EDITOR_PASSWORD)

            # The image to use for testing
            f = urllib.request.urlopen("http://www.lookup.cam.ac.uk/images/ibis.jpg")
            imageData = f.read()
            f.close()

            # Delete any test images from previous runs
            inst = im.getInst("CSTEST", "jpegPhoto")
            for attr in inst.attributes:
                im.deleteAttribute("CSTEST", attr.attrid,
                                   "Unit test: delete existing test photos")

            inst = im.getInst("CSTEST", "jpegPhoto")
            self.assertEqual(0, len(inst.attributes))

            # Test adding a new photo
            newAttr = IbisAttribute()
            newAttr.scheme = "jpegPhoto"
            newAttr.binaryData = imageData
            newAttr.comment = "Unit testing"

            newAttr = im.addAttribute("CSTEST", newAttr, 0, True,
                                      "Unit test: add a test photo")
            self.assertIsNone(newAttr.value)
            self.assertEqual(imageData, newAttr.binaryData)
            self.assertEqual("Unit testing", newAttr.comment)

            inst = im.getInst("CSTEST", "jpegPhoto")

            found = False
            for attr in inst.attributes:
                self.assertEqual(newAttr.attrid, attr.attrid)
                self.assertIsNone(attr.value)
                self.assertEqual(imageData, attr.binaryData)
                self.assertEqual(newAttr.comment, attr.comment)
                self.assertEqual(newAttr.effectiveFrom, attr.effectiveFrom)
                self.assertEqual(newAttr.effectiveTo, attr.effectiveTo)
                self.assertFalse(found)
                found = True
            self.assertTrue(found)

            # Test deleting the new photo
            deleted = im.deleteAttribute("CSTEST", newAttr.attrid,
                                         "Unit test: delete photo")
            self.assertTrue(deleted)

            inst = im.getInst("CSTEST", "jpegPhoto")
            self.assertEqual(0, len(inst.attributes))
        finally:
            conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

    def test_modified_insts(self):
        # Note: transactions 2..1047 are the same on Lookup and lookup-test
        insts = im.modifiedInsts(491, 492, None, False, False, False, None)

        self.assertEqual(1, len(insts))
        self.assertEqual("AUT", insts[0].instid)

        insts = im.modifiedInsts(438, 439, None, False, False, False, None)
        self.assertEqual(0, len(insts))

        insts = im.modifiedInsts(438, 439, None, True, False, False, None)
        self.assertEqual(1, len(insts))
        self.assertEqual("SPVSR04", insts[0].instid)

        insts = im.modifiedInsts(764, 765, "IUSCMED", False, False, False, None)
        self.assertEqual(1, len(insts))
        self.assertEqual("IUSCMED", insts[0].instid)

        insts = im.modifiedInsts(764, 765, "IUSCMED2", False, False, False, None)
        self.assertEqual(0, len(insts))

        insts = im.modifiedInsts(45, 46, None, False, False, False, None)
        self.assertEqual(0, len(insts))

        insts = im.modifiedInsts(45, 46, None, False, True, False, None)
        self.assertEqual(1, len(insts))
        self.assertEqual("Clare Hall", insts[0].name)

    # --------------------------------------------------------------------
    # Group tests.
    # --------------------------------------------------------------------

    def test_no_such_group(self):
        group = gm.getGroup("654321")
        self.assertIsNone(group)
        group = gm.getGroup("no-such-group-exists")
        self.assertIsNone(group)
        group = gm.getGroup("x4fu89fd")
        self.assertIsNone(group)

    def test_get_group_details(self):
        group1 = gm.getGroup("100656")
        group2 = gm.getGroup("cs-editors")

        self.assertEqual("100656", group2.groupid)
        self.assertEqual("cs-editors", group1.name)
        self.assertEqual(group1.description, group2.description)

    def test_get_group_members(self):
        group = gm.getGroup("cs-editors", "all_members")
        people = gm.getMembers("cs-editors")
        direct_people = gm.getDirectMembers("cs-editors")

        self.assertTrue(len(people) > 5)
        self.assertTrue(len(group.members) == len(people))
        self.assertTrue(len(direct_people) <= len(people))
        for i in range(0, len(people)):
            p1 = group.members[i]
            p2 = people[i]
            self.assertEqual(p1.identifier.scheme, p2.identifier.scheme)
            self.assertEqual(p1.identifier.value, p2.identifier.value)
            self.assertEqual(p1.displayName, p2.displayName)
        for p1 in direct_people:
            found = False
            for p2 in people:
                if p2.identifier.scheme == p1.identifier.scheme and \
                   p2.identifier.value == p1.identifier.value:
                    self.assertFalse(found)
                    found = True
            self.assertTrue(found)

    def test_get_group_cancelled_members(self):
        people = gm.getCancelledMembers("cs-members")

        self.assertTrue(len(people) > 30)

        found = False
        for person in people:
            if person.identifier.scheme == "crsid" and\
               person.identifier.value == "dar54":
                self.assertEqual("Dr D.A. Rasheed", person.registeredName)
                found = True
        self.assertTrue(found)

    def test_get_group_insts(self):
        group = gm.getGroup("uis-editors", "owning_insts,manages_insts")
        self.assertEqual("UIS", group.owningInsts[0].instid)
        self.assertEqual("UIS", group.managesInsts[0].instid)
        self.assertTrue(group.owningInsts[0] == group.managesInsts[0])

    def test_get_group_members_of_inst(self):
        group = gm.getGroup("uis-members", "members_of_inst")
        self.assertEqual("UIS", group.membersOfInst.instid)
        self.assertEqual("University Information Services", group.membersOfInst.name)

    def test_get_group_mgrs(self):
        group = gm.getGroup("cs-editors", "managed_by_groups.managed_by_groups")
        self.assertEqual("cs-managers", group.managedByGroups[0].name)
        self.assertEqual("cs-managers", group.managedByGroups[0].managedByGroups[0].name)
        self.assertTrue(group.managedByGroups[0] == group.managedByGroups[0].managedByGroups[0])

    def test_list_groups(self):
        groups = gm.listGroups("cs-editors,000000,cs-members", "managed_by_groups")
        self.assertEqual(3, len(groups))
        self.assertEqual("000000", groups[0].groupid)
        self.assertEqual("cs-editors", groups[1].managedByGroups[0].name)
        self.assertEqual("cs-managers", groups[2].managedByGroups[0].name)

    def test_group_search(self):
        groups = gm.search("Editors group for UIS", False, False, 0, 100)
        self.assertEqual("uis-editors", groups[0].name)

        groups = gm.search("information services test accounts members", False, False,
                           0, 1, None, "all_members")
        self.assertEqual(1, len(groups))
        self.assertEqual("uistest-members", groups[0].name)
        self.assertTrue(len(groups[0].members) > 10)
        self.assertEqual("abc123", groups[0].members[0].identifier.value)

    def test_group_search_count(self):
        count = gm.searchCount("maths editors")
        self.assertEqual(6, count)

    def test_group_lql_search(self):
        groups = gm.search("group: title='Editors group for \"UIS\"'",
                           False, False, 0, 100, None, None)
        self.assertEqual("uis-editors", groups[0].name)

        groups = gm.search("group: uistest-members", False, False,
                           0, 1, None, "all_members")
        self.assertEqual(1, len(groups))
        self.assertEqual("uistest-members", groups[0].name)
        self.assertTrue(len(groups[0].members) > 10)
        self.assertEqual("abc123", groups[0].members[0].identifier.value)

        groups = gm.search("group: biotec-editors", False, False,
                           0, 1, None, None)
        self.assertEqual(0, len(groups))

        groups = gm.search("group: biotec-editors", False, True,
                           0, 1, None, None)
        self.assertEqual(1, len(groups))

    def test_group_lql_search_count(self):
        count = gm.searchCount("group: biotec-editors", False, False)
        self.assertEqual(0, count)

        count = gm.searchCount("group: biotec-editors", False, True)
        self.assertEqual(1, count)

    def test_edit_group_members(self):
        if not run_edit_tests:
            return

        try:
            conn.set_username(LOOKUP_TEST_SERVER_EDITOR_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_EDITOR_PASSWORD)

            # Initial members of cstest-members (should include mug99)
            originalMembers = gm.getDirectMembers("cstest-members")

            found = False
            for person in originalMembers:
                if person.identifier.scheme == "crsid" and \
                   person.identifier.value == "mug99":
                    self.assertFalse(found)
                    found = True
            self.assertTrue(found)

            # Test removing mug99, and a non-existent USN
            newMembers = gm.updateDirectMembers("cstest-members", None, "mug99,usn/123456789",
                                                "Unit test: remove mug99 from cstest-members")
            self.assertEqual(len(originalMembers)-1, len(newMembers))

            for person in newMembers:
                self.assertFalse(person.identifier.scheme == "crsid" and
                                 person.identifier.value == "mug99")

            # Test adding mug99 back and removing all other members
            newMembers = gm.updateDirectMembers(
                "cstest-members", "mug99", "all-members",
                "Unit test: make mug99 the only member of cstest-members")
            self.assertEqual(1, len(newMembers))
            self.assertEqual("crsid", newMembers[0].identifier.scheme)
            self.assertEqual("mug99", newMembers[0].identifier.value)

            # Test restoring all the original members (including mug99 again)
            newIds = ""
            for person in originalMembers:
                newIds += person.identifier.scheme+"/"+person.identifier.value+","

            newMembers = gm.updateDirectMembers("cstest-members", newIds, None,
                                                "Unit test: restore members of cstest-members")
            self.assertEqual(len(originalMembers), len(newMembers))

            for i in range(0, len(originalMembers)):
                p1 = originalMembers[i]
                p2 = newMembers[i]
                self.assertEqual(p1.identifier.scheme, p2.identifier.scheme)
                self.assertEqual(p1.identifier.value, p2.identifier.value)
        finally:
            conn.set_username(LOOKUP_TEST_SERVER_VIEWER_USERNAME)
            conn.set_password(LOOKUP_TEST_SERVER_VIEWER_PASSWORD)

    def test_modified_groups(self):
        # Note: transactions 2..1047 are the same on Lookup and lookup-test
        groups = gm.modifiedGroups(492, 493, None, False, False, None)

        self.assertEqual(1, len(groups))
        self.assertEqual("100426", groups[0].groupid)
        self.assertEqual("maths-intakes-editors", groups[0].name)

        groups = gm.modifiedGroups(487, 488, None, False, False, None)
        self.assertEqual(0, len(groups))

        groups = gm.modifiedGroups(487, 488, None, True, False, None)
        self.assertEqual(1, len(groups))
        self.assertEqual("100855", groups[0].groupid)
        self.assertEqual("cstest-foofoo", groups[0].name)

        groups = gm.modifiedGroups(743, 744, "100259", False, False, None)
        self.assertEqual(1, len(groups))
        self.assertEqual("100259", groups[0].groupid)
        self.assertEqual("biol-managers", groups[0].name)

        groups = gm.modifiedGroups(743, 744, "biol-managers", False, False, None)
        self.assertEqual(1, len(groups))
        self.assertEqual("100259", groups[0].groupid)
        self.assertEqual("biol-managers", groups[0].name)

        groups = gm.modifiedGroups(743, 744, "100260", False, False, None)
        self.assertEqual(0, len(groups))
        groups = gm.modifiedGroups(743, 744, "biol-editors", False, False, None)
        self.assertEqual(0, len(groups))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(IbisUnitTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
